const requestURL = 'https://jsonplaceholder.typicode.com/users';
const xhr = new XMLHttpRequest();
let users;
let selectId = document.getElementById('select_id');
let selectKey = document.getElementById('select_key');
let enterValue = document.getElementById('enter_value');
let editButton = document.getElementById('edit_btn');
let deleteButton = document.getElementById('delete_btn');

xhr.open('GET', requestURL);

xhr.onload = () => {
    users = JSON.parse(xhr.response);
    console.log(users);
}

xhr.send()

editButton.addEventListener('click', evt => {
    evt.preventDefault();

    for (let i = 0; i < users.length; i++) {
        if (users[i].id === Number(selectId.value)) {
            users[i][selectKey.value] = enterValue.value;
        }
    }

    console.log(users)
});

deleteButton.addEventListener('click', evt => {
    evt.preventDefault();

    for (let i = 0; i < users.length; i++) {
        if (users[i].id === Number(selectId.value)) {
            users.splice(i, 1);
        }
    }

    console.log(users);
});
