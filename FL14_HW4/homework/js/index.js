class Deck {
    constructor() {
        this.cards = [
            { rank: "1", suit: "Spades" },
            { rank: "1", suit: "Clubs" },
            { rank: "1", suit: "Diamonds" },
            { rank: "1", suit: "Hearts" },
            { rank: "2", suit: "Spades" },
            { rank: "2", suit: "Clubs" },
            { rank: "2", suit: "Diamonds" },
            { rank: "2", suit: "Hearts" },
            { rank: "3", suit: "Spades" },
            { rank: "3", suit: "Clubs" },
            { rank: "3", suit: "Diamonds" },
            { rank: "3", suit: "Hearts" },
            { rank: "4", suit: "Spades" },
            { rank: "4", suit: "Clubs" },
            { rank: "4", suit: "Diamonds" },
            { rank: "4", suit: "Hearts" },
            { rank: "5", suit: "Spades" },
            { rank: "5", suit: "Clubs" },
            { rank: "5", suit: "Diamonds" },
            { rank: "5", suit: "Hearts" },
            { rank: "6", suit: "Spades" },
            { rank: "6", suit: "Clubs" },
            { rank: "6", suit: "Diamonds" },
            { rank: "6", suit: "Hearts" },
            { rank: "7", suit: "Spades" },
            { rank: "7", suit: "Clubs" },
            { rank: "7", suit: "Diamonds" },
            { rank: "7", suit: "Hearts" },
            { rank: "8", suit: "Spades" },
            { rank: "8", suit: "Clubs" },
            { rank: "8", suit: "Diamonds" },
            { rank: "8", suit: "Hearts" },
            { rank: "9", suit: "Spades" },
            { rank: "9", suit: "Clubs" },
            { rank: "9", suit: "Diamonds" },
            { rank: "9", suit: "Hearts" },
            { rank: "10", suit: "Spades" },
            { rank: "10", suit: "Clubs" },
            { rank: "10", suit: "Diamonds" },
            { rank: "10", suit: "Hearts" },
            { rank: "11", suit: "Spades" },
            { rank: "11", suit: "Clubs" },
            { rank: "11", suit: "Diamonds" },
            { rank: "11", suit: "Hearts" },
            { rank: "12", suit: "Spades" },
            { rank: "12", suit: "Clubs" },
            { rank: "12", suit: "Diamonds" },
            { rank: "12", suit: "Hearts" },
            { rank: "13", suit: "Spades" },
            { rank: "13", suit: "Clubs" },
            { rank: "13", suit: "Diamonds" },
            { rank: "13", suit: "Hearts" }
        ];
    }

    get count() {
        return this.cards.length;
    }

    shuffle() {
        for (let i = 0; i < this.cards.length; i++) {
            let randomPosition = Math.floor(Math.random() * Math.floor(52))
            let temp = this.cards[i];
            this.cards[i] = this.cards[randomPosition];
            this.cards[randomPosition] = temp;
        }
        return this.cards;
    }

    draw(n) {
        return this.cards.splice(this.count - n, n);
    }

}

class Card {
    constructor() {
        this.suit = ["Hearts", "Diamonds", "Clubs", "Spades"];
        this.rank = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"];
    }

    get isFaceCard() {
        if (this.rank === 1 || this.rank > 10) {
            return true;
        }
        return false;
    }

    toString() {
        let humanReadableRank;
        if (this.rank === "1") {
            humanReadableRank = "Ace";
        } else if (this.rank === "11") {
            humanReadableRank = "Jack";
        } else if (this.rank === "12") {
            humanReadableRank = "Queen";
        } else if (this.rank === "13") {
            humanReadableRank = "King";
        } else {
            humanReadableRank = this.rank;
        }
        return `${humanReadableRank} of ${this.suit}`
    }

    compare(cardOne, cardTwo) {
        if (cardOne.rank > cardTwo.rank) {
            return "First player gets 1 point!"
        } else if (cardOne.rank === cardTwo.rank) {
            return "Nobody gets a point."
        } else if (cardOne.rank < cardTwo.rank) {
            return "Second player gets 1 point!"
        }
    }
}

class Player {
    constructor(name) {
        this.name = name;
        this._wins = 0;
    }

    get wins() {
        return this._wins;
    }

    Play(playerOne, playerTwo) {
        const deckOne = new Deck();
        const deckTwo = new Deck();
        const card = new Card();

        let playerOnePoints = 0;
        let playerTwoPoints = 0;
        let Winner;
        deckOne.shuffle();
        deckTwo.shuffle();

        let compareResult = card.compare(deckOne.draw(1), deckTwo.draw(1));

        console.log(compareResult);
        if (compareResult === "First player gets 1 point!") {
            playerOnePoints++;
        } else if (compareResult === "Second player gets 1 point!") {
            playerTwoPoints++;
        }

        if (playerOnePoints > playerTwoPoints) {
            Winner = playerOne.name;
        } else if (playerOnePoints < playerTwoPoints) {
            Winner = playerTwo.name;
        }
        return `${Winner} wins ${playerOnePoints} to ${playerTwoPoints}`;
    }
}
