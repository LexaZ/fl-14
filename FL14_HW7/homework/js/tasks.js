const maxElement = array => Math.max(...array);

const copyArray = array => {
    let newArray = [...array];
    return newArray;
}

const addUniqueId = object => {
    let newObject = {};
    let id = Symbol('id');
    const rangeOfNumbers = 100;
    Object.assign(newObject, object);
    newObject[id] = Math.floor(Math.random() * rangeOfNumbers);
    return newObject;
}

const regroupObject = object => {
    let newObject = { user: {} };
    let { id, age, university } = object.details;
    let { name } = object;

    newObject.university = university;
    newObject.user.age = age;
    newObject.user.firstName = name;
    newObject.user.id = id;
    return newObject;
}

const findUniqueElements = array => {
    let uniqueElements = [];
    for (let element of array) {
        if (!uniqueElements.includes(element)) {
            uniqueElements.push(element)
        }
    }
    return uniqueElements;
}

const hideNumber = phoneNumber => {
    const visibleNumbersQuantity = 4;
    return phoneNumber.slice(phoneNumber.length - visibleNumbersQuantity).padStart(phoneNumber.length, '*');
}

const error = () => {
    throw Error('Missing property')
}
const add = (x = error(), y = error()) => {
    return x + y;
}
