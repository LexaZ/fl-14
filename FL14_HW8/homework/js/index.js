const $list = $(".list");
const $input = $("#add-input");
const $add = $("#add-submit");
const $search = $("#search-task-input");

const todos = [
  {
    text: "Buy milk",
    done: false
  },
  {
    text: "Play with dog",
    done: true
  }
];

if (localStorage.getItem('todos') === null) {
  localStorage.setItem("todos", JSON.stringify(todos));
}

let localTodos = JSON.parse(localStorage.getItem("todos"));

(function ($) {
  $.fn.addTask = function () {
    let newToDo = {
      text: $input.val(),
      done: false
    }

    localTodos.push(newToDo);
    localStorage.setItem("todos", JSON.stringify(localTodos));

    return this;
  };

  $.fn.todoHasText = function (text) {
    return this.find(".item-text").text().includes(text);
  };
}(jQuery));

$add.click(function () {
  $(this).addTask();
});

for (let i = 0; i < localTodos.length; i++) {
  let item = localTodos[i];
  $list.append(
    `<li class='item'>
        <span class='item-text'>${item.text}</span>
        <button class='item-remove'>Remove</button>
      </li>`);
  let $newItem = $("li:last-of-type");
  let $newItemText = $("li:last-of-type span");
  let $newItemRemove = $("li:last-of-type button");
  if (item.done === true) {
    $newItemText.addClass("done");
  }
  $newItemText.click(function () {
    $newItemText.toggleClass("done");
    item.done = item.done === true ? false : true;
    localStorage.setItem("todos", JSON.stringify(localTodos));
  });
  $newItemRemove.addClass("item-remove");
  $newItemRemove.click(function () {
    $newItem.remove();
    localTodos.splice(i, 1);
    localStorage.setItem("todos", JSON.stringify(localTodos));
    location.reload();
  });
  console.log(localTodos.length)
}

$search.keyup(function (event) {
  const searchedText = event.target.value.trim();
  if (searchedText.length === 0) {
    $list.children().show();
  } else {
    $list.children().each(function () {
      if ($(this).todoHasText(searchedText)) {
        $(this).show();
      } else {
        $(this).hide();
      }
    });
  }
});