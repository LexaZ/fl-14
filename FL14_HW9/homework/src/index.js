const rock = document.querySelector('.rock');
const paper = document.querySelector('.paper');
const scissors = document.querySelector('.scissors');
const turn = document.querySelector('.turn');

let enemyFigure, myFigure, enemyScore, myScore;


function opponentFigure() {
    let enemyFigureNumber = Math.floor(Math.random() * Math.floor(3));
    if (enemyFigureNumber === 0) {
        enemyFigure = 'rock'
    } else if (enemyFigureNumber === 1) {
        enemyFigure = 'paper'
    } else if (enemyFigureNumber === 2) {
        enemyFigure = 'scissors'
    }
    return enemyFigure;
}

rock.addEventListener('click', (evt) => {
    evt.preventDefault();
    myFigure = 'rock';
    opponentFigure();
    startGame();
});

function startGame() {
    if (opponentFigure === 'rock' && myFigure === 'paper') {
        enemyScore++;
    }
}
