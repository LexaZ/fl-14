//Task 1
const isEquals = (valueFirst, valueSecond) => valueFirst === valueSecond;

//Task 2
const numberToString = number => String(number);

//Task 3
const storeNames = (...names) => names;

//Task 4
const getDivision = (valueFirst, valueSecond) => {
    let division = valueFirst / valueSecond;
    if (valueFirst < valueSecond) {
        division = valueSecond / valueFirst;
    }
    return division;
}

//Task 5
const negativeCount = (array) => {
    let negativeQuantity = 0;
    for (let i = 0; i < array.length; i++) {
        if (array[i] < 0) {
            negativeQuantity++;
        }
    }
    return negativeQuantity;
}

//Task 6
const letterCount = (fullString, fragment) => {
    const regexp = new RegExp(`${fragment}`, `g`);
    let repeatsQuantity = (fullString.match(regexp) || []).length;
    return repeatsQuantity;
}

//Task 7
const countPoints = (array) => {
    let totalScore = 0;
    const pointsForVictory = 3;
    for (let i = 0; i < array.length; i++) {
        let ourTeamPoints = Number(array[i].split(':')[0]);
        let enemyTeamPoints = Number(array[i].split(':')[1]);
        if (ourTeamPoints > enemyTeamPoints) {
            totalScore += pointsForVictory;
        } else if (ourTeamPoints === enemyTeamPoints) {
            totalScore++;
        }
    }
    return totalScore;
}