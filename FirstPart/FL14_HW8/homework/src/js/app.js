const startButton = document.querySelector('.start-btn');
const skipButton = document.querySelector('.skip-btn');
let hiddenParts = document.querySelectorAll('.hide');
const options = document.querySelectorAll('.answer-btn');
const questionField = document.querySelector('.question-field');
let qs = JSON.parse(localStorage.getItem('questions'));
let totalPrize = document.querySelector('.total-prize');
let currentRoundPrize = document.querySelector('.current-round-prize');
let endOfGame = document.querySelector('.end-of-game');
let score = 0;
let thisRoundScore = 100;
let completedAnswers = [];
let indexOfQuestion;
let two = 2;
let oneHundred = 100;
let million = 1000000;

for (let i = 0; i < options.length; i++) {

    let action = function () {

        if (i === qs[indexOfQuestion].correct) {
            score += thisRoundScore;
            thisRoundScore *= two;
            if (score > million) {
                for (let i = 0; i < hiddenParts.length; i++) {
                    hiddenParts[i].classList.add('hide');
                }
                endOfGame.classList.remove('hide');
                endOfGame.innerHTML = 'Congratulations! You won ' + score;
            }

            randomQuestion();
        } else {
            gameOver();
        }
    }

    options[i].addEventListener('click', action);
}

const gameOver = () => {

    for (let i = 0; i < hiddenParts.length; i++) {
        hiddenParts[i].classList.add('hide');
    }
    endOfGame.classList.remove('hide');
    endOfGame.innerHTML = 'Game over! Your score: ' + score;
}

startButton.addEventListener('click', () => {
    for (let i = 0; i < hiddenParts.length; i++) {
        hiddenParts[i].classList.remove('hide');
    }
    endOfGame.classList.add('hide');
    score = 0;
    thisRoundScore = oneHundred;
    totalPrize.innerHTML = 'Total prize: ' + score;
    currentRoundPrize.innerHTML = 'Prize on current round: ' + thisRoundScore;
    completedAnswers = [];
    randomQuestion();
    skipButton.disabled = false;
});

skipButton.addEventListener('click', () => {
    randomQuestion();
    skipButton.disabled = true;
});

const randomQuestion = () => {
    let randomNumber = Math.floor(Math.random() * qs.length);
    let duplicate = false;

    if (completedAnswers.length >= 0) {
        completedAnswers.forEach(item => {
            if (item === randomNumber) {
                duplicate = true;
            }
        });
        if (duplicate) {
            randomQuestion();
        } else {
            indexOfQuestion = randomNumber;
            loadQuestion();
        }
    }

    completedAnswers.push(indexOfQuestion);
}

const loadQuestion = () => {
    questionField.innerHTML = qs[indexOfQuestion].question;
    totalPrize.innerHTML = 'Total prize: ' + score;
    currentRoundPrize.innerHTML = 'Prize on current round: ' + thisRoundScore;
    for (let i = 0; i < options.length; i++) {
        options[i].innerHTML = qs[indexOfQuestion].content[i];
    }
}




