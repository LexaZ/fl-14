let word = prompt('Enter your word:', '').trim();
let two = 2;
let middle = Math.floor(word.length/two)
if (word === '') {
    alert('Invalid value');
} else if (word.length % two !== 0) {
    alert('Middle character: ' + word[middle]);
} else {
    if (word[middle-1] === word[middle]) {
        alert('Middle characters are the same');
    } else {
        alert('Middle characters: ' + word[middle-1] + word[middle]);
    }
}