let batteriesAmount = prompt('Amount of batteries:', '').trim();
let defectivePercent = prompt('Percentage of defective batteries [0-100]', '').trim();
let hundredPercent = 100;
let deffectiveAmount = batteriesAmount / hundredPercent * defectivePercent;
let workingAmount = batteriesAmount - deffectiveAmount;
let numberRounding = 2;
if (isNaN(batteriesAmount) || batteriesAmount < 0 || batteriesAmount === '' || 
    isNaN(defectivePercent) || defectivePercent < 0 || defectivePercent === '' || defectivePercent > hundredPercent) {
    alert('Invalid input data'); 
} else {
alert(`Amount of batteries: ${batteriesAmount} 
Defective rate: ${defectivePercent}% 
Amount of defective batteries: ${deffectiveAmount.toFixed(numberRounding)} 
Amount of working batteries:  ${workingAmount.toFixed(numberRounding)}`);
}
