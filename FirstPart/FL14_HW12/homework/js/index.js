function visitLink(path) {
	let firstPageVisits, secondPageVisits, thirdPageVisits;
	if (path === 'Page1') {
		if (localStorage.getItem('first') == null) {
			firstPageVisits = 1;
		}
		else firstPageVisits = Number(localStorage.getItem('first')) + 1;
		localStorage.setItem('first', firstPageVisits);
	} else if (path === 'Page2') {
		if (localStorage.getItem('second') == null) secondPageVisits = 1;
		else secondPageVisits = Number(localStorage.getItem('second')) + 1;
		localStorage.setItem('second', secondPageVisits);
	} else if (path === 'Page3') {
		if (localStorage.getItem('third') == null) thirdPageVisits = 1;
		else thirdPageVisits = Number(localStorage.getItem('third')) + 1;
		localStorage.setItem('third', thirdPageVisits);
	}
}

function viewResults() {
	let content = document.getElementById('content');
	let ul = document.createElement('ul');
	let liFirst = document.createElement('li');
	let liSecond = document.createElement('li');
	let liThird = document.createElement('li');

	if (content.lastChild == document.getElementsByTagName('li')) {
		console.log('true')
	} else {
		console.log('false')
	}
	
	content.appendChild(ul);
	ul.appendChild(liFirst);
	ul.appendChild(liSecond);
	ul.appendChild(liThird);
	liFirst.innerHTML = 'You visited page for ' + localStorage.getItem('first') + ' time(s)';
	liSecond.innerHTML = 'You visited page for ' + localStorage.getItem('second') + ' time(s)';
	liThird.innerHTML = 'You visited page for ' + localStorage.getItem('third') + ' time(s)';
}

