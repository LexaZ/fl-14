let expr, result;
while (true) {
    expr = prompt('Enter your expression: ')
    if(expr === null) break;

    try {
        result = eval(expr);
        if(isNaN(result)) {
            throw new Error('The result is undefined');
        }

        break;
    } catch (e) {
        alert('Error: ' + e.message + ', try again');
    }
}

alert(result);