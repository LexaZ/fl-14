//Task 1
const getAge = (birthdayDate) => {
    let today = new Date();
    let thisYearBirthday = new Date(today.getFullYear(), birthdayDate.getMonth(), birthdayDate.getDate());
    let age = today.getFullYear() - birthdayDate.getFullYear();
    if (today < thisYearBirthday) {
        age--
    }
    return age;
}

//Task 2
const getWeekDay = (date) => {
    let d = new Date(date);
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    let day = d.getDay();
    for (let i = 0; i < days.length; i++) {
        if (i === day) {
            return days[i]
        }
    }
}

//Task 3
const getProgrammersDay = (year) => {
    let date = new Date(year, 0);
    const programmersDayinYear = 256;
    let weekday = getWeekDay(date.setDate(programmersDayinYear));
    let month;
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    for (let i = 0; i < months.length; i++) {
        if (i === date.getMonth()) {
            month = months[i]
        }
    }
    return `${date.getDate()} ${month}, ${date.getFullYear()} (${weekday})`;
}

//Task 4
const howFarIs = (specifiedWeekday) => {
    let now = new Date();
    let currentWeekday = getWeekDay(now);
    specifiedWeekday = specifiedWeekday.charAt(0).toUpperCase() + specifiedWeekday.slice(1);

    if (specifiedWeekday === currentWeekday) {
        return `Hey, today is ${specifiedWeekday} =)`
    }

    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    for (let i = 0; i < days.length; i++) {
        if (days[i] === specifiedWeekday) {
            for (let j = 0; j < days.length; j++) {
                if (days[j] === currentWeekday) {
                    let daysToSpecifiedWeekday = i - j;
                    if (i < j) {
                        daysToSpecifiedWeekday = j - i;
                    }
                    return `It's ${daysToSpecifiedWeekday} day(s) left till ${specifiedWeekday}.`
                }
            }
        }
    }
}

//Task 5
const isValidIdentifier = input => Boolean(input.match(/^[^0-9\s][0-9a-zA-z_$]*$/g));

//Task 6
const capitalize = input => input.replace(/\b\w/g, newString => newString.charAt(0).toUpperCase() + newString.slice(1));

//Task 7
const isValidAudioFile = input => Boolean(input.match(/^([a-zA-Z])+(.mp3|.alac|.aac|.flac)$/g));

//Task 8
const getHexadecimalColors = input => input.match(/#([a-f0-9]{3}){1,2}\b/gi);

//Task 9
const isValidPassword = input => Boolean(input.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/g));

//Task 10
const addThousandsSeparators = input => String(input).replace(/\B(?=(\d{3})+(?!\d))/g, ',');