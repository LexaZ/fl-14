let two = 2;
let year = 2020;

//Task 1
function convert(...args) {
    for (let i = 0; i < args.length; i++) {
        if (typeof args[i] === 'number') {
            args[i] = args[i] + '';
        } else if (typeof args[i] === 'string') {
            args[i] = Number.parseInt(args[i]);
        }
    }
    return args;
}
// console.log(convert('1', 2, 3, '4'));

//Task 2
function executeforEach(arr, fn) {
    for (let i = 0; i < arr.length; i++) {
        arr[i] = fn(arr[i]);
    }
}
// executeforEach([1, 2, 3], function (el) { console.log(el * 2) });

//Task 3
function mapArray(arr, fn) {
    let mappedArray = [];
    executeforEach(arr, function (el) {
        if (typeof el === 'string') {
            el = Number.parseInt(el)
        }
        mappedArray.push(fn(el));
    });
    return mappedArray;
}
// console.log(mapArray([2, '5', 8], function (el) { return el + 3; }));

//Task 4
function filterArray(arr, fn) {
    let filteredArray = [];
    executeforEach(arr, function (el) {
        if (el % two === 0) {
            filteredArray.push(fn(el));
        }
    });
    return filteredArray;
}
// console.log(filterArray([2, 5, 8], function(el) { return el; }));

//Task 5
function getValuePosition(arr, value) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === value) {
            return i + 1;
        }
    }
    return false;
}
// console.log(getValuePosition([2, 5, 8], 8));
// console.log(getValuePosition([12, 4, 6], 1));

//Task 6
function flipOver(str) {
    let newStr = '';
    for (let i = str.length-1; i >=0 ; i--) {
        newStr += str[i];
    }
    console.log(newStr);
}
// flipOver('hey world')

//Task 7
function makeListFromRange([start, end]) {
    let newArr = [];
    for (let i = start; i<=end;i++) {
        newArr.push(i);
    }
    console.log(newArr);
}
// makeListFromRange([2, 7]);

//Task 8
const fruits = [   
    { name: 'apple', weight: 0.5 },   
    { name: 'pineapple', weight: 2 } 
];   

function getArrayOfKeys(arr, anyKey) {
    let keys = [];
    executeforEach(arr, function(obj) {
        keys.push(obj[anyKey]);
    });
    return keys;
}
// console.log(getArrayOfKeys(fruits, 'name'));

// Task 9
const basket = [   
    { name: 'Bread', weight: 0.3 },   
    { name: 'Coca-Cola', weight: 0.5 },   
    { name: 'Watermelon', weight: 8 } 
]; 

function getTotalWeight(arr) {
    let sum = 0;
    executeforEach(arr, function(obj) {
        sum += obj['weight'];
    });
    return sum;
}
// console.log(getTotalWeight(basket));

//Task 10
const date = new Date(year, 0, two); 
function getPastDay(date, day) {
    date.setDate(date.getDate()-day);
    console.log(date);
}
// getPastDay(date, 1); 
// getPastDay(date, 2); 
// getPastDay(date, 365); 

